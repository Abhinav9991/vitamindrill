const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


// 1 Get all items that are available 
const availableItems=items.filter(item=>{
    if (item.available===true) {
        
        return true 
    }
    else{
        return false
    }
})
// console.log(availableItems);

//2.  Get all items containing only Vitamin C.
const vitaminCItems=items.filter(item=>{
    if (item.contains==="Vitamin C") {
        return true
    } else {
        return false
    }
})
// console.log(vitaminCItems);

// 3. Get all items containing Vitamin A.
const vitaminAItems=items.filter(item=>{
    if(item.contains.includes("Vitamin A")){
        return true
    }
    else return false
})
console.log(vitaminAItems);